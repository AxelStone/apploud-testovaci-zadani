import fetch from "node-fetch";
import { topGroupId, accessToken, baseUrl, perPage } from "./config.js";
import * as fs from 'fs';

const headers = {
  "Content-Type": "application/json",
  Authorization: `Bearer ${accessToken}`,
};

const accessRights = {
  0: 'No access',
  10: 'Guest',
  20: 'Reporter',
  30: 'Developer',
  40: 'Maintainer',
  50: 'Owner'
}

let groupList = [];
let projectList = [];
let userList = [];

console.log("loading group list...");

// get top group detail
fetch(`${baseUrl}/groups/${topGroupId}`, { headers: headers })
  .then((res) => res.json())
  .then((group) => {

    groupList.push(group);
    projectList = projectList.concat(group.projects)

    // get subgroups list
    return fetch(`${baseUrl}/groups/${topGroupId}/descendant_groups?per_page=${perPage}`, { headers: headers })
      .then(r => r.json())

  // get detail of every subgroup
  }).then(subgroups => { 

    console.log("loading group details and projects...");

    let promises = [];    
    subgroups.forEach(sg => {
      promises.push(
        fetch(`${baseUrl}/groups/${sg.id}`, { headers: headers })
          .then(r => r.json())
          .then(subgroup => {
            groupList.push(subgroup)
            projectList = projectList.concat(subgroup.projects)
          })
      )
    })

    return Promise.all(promises)      

  // get members of every group and update userList
  }).then(res => {    
    
    console.log("loading members of groups...");

    let promises = []; 
    groupList.forEach(group => {
      promises.push(
        promises.push(
          loadAllMembers(group)
        )      
      )
    })

    return Promise.all(promises) 

  // get members of every project and update userList
  }).then(res => {

    console.log("loading members of projects...");

    let promises = []; 
    projectList.forEach(project => {
      promises.push(
        loadAllMembers(project)
      )
    })

    return Promise.all(promises) 

  // output userList
  }).then(res => { 
    let content = 'APPLOUD - GITLAB USER LIST\n'
    content += `Date: ${(new Date()).toUTCString()} \n`;
    content += '---\n'
    userList.forEach(u => {
      let groups = [], projects = [];
      u.groups.forEach(g => {
        groups.push(`${g.full_path} (${accessRights[g.access_level]})`)
      })
      u.projects.forEach(p => {
        projects.push(`${p.path_with_namespace} (${accessRights[p.access_level]})`)
      })
      content += `${u.name} (@${u.username})\n`
      content += `Groups [${groups.join(', ')}]\n`
      content += `Projects [${projects.join(', ')}]\n\n`
    })
    content = content.slice(0, -1);
    content += '---\n\n'
    content += `Total user count: ${userList.length}`

    console.log('--------------------------')
    console.log(content)
    fs.writeFile('result.txt', content, err => {});
    console.log('--------------------------')
    console.log('See result.txt file for complete output...')

  }).catch((err) => console.error(err));

/**
 * Add new member to the list or merge his projects or groups
 * @param {*} members 
 * @param {*} groupOrProject 
 */
function updateUserList(members, groupOrProject) {
  members.forEach(m => {
    const existingUser = userList.find(u => u.id == m.id)

    // save user access level for this group or project
    groupOrProject.access_level = m.access_level

    // merge projects or groups if user already is in the list
    if (existingUser) {
      groupOrProject.projects !== undefined ? 
        existingUser.groups.push(groupOrProject) : 
        existingUser.projects.push(groupOrProject)
    
    // add new user to the list
    } else {
      userList.push({
        ...m,
        groups: groupOrProject.projects !== undefined ? [groupOrProject] : [],
        projects: groupOrProject.projects == undefined ? [groupOrProject] : []
      })
    }
  })
}

/**
 * Recursively load all members of group or project on multiple pages
 * @param {*} groupOrProject 
 */
function loadAllMembers(groupOrProject) {

  const type = groupOrProject.projects !== undefined ? 'groups' : 'projects'
  let nextPage = null;

  return fetch(`${baseUrl}/${type}/${groupOrProject.id}/members?per_page=${perPage}`, { headers: headers })
    .then(r => { 
      nextPage = parseInt(r.headers.get('x-next-page'))
      return r.json()
    }).then(members => {
      updateUserList(members, groupOrProject);
      if (nextPage) {
        loadAllMembers(groupOrProject);
      }
    })  
}